//
//  AudioClip.hpp
//  MVVJ
//
//  Created by Joseph Chow on 2/27/16.
//
//

#ifndef AudioClip_hpp
#define AudioClip_hpp

#include "cinder/audio/audio.h"

namespace moca {
    
    typedef std::shared_ptr<class AudioClip> AudioClipRef;
    
    class AudioClip {
        
        ci::audio::GainNodeRef				mGain;
        ci::audio::BufferPlayerNodeRef		mBufferPlayerNode;
    public:
        AudioClip();
    
        static AudioClipRef create(){
            return AudioClipRef(new AudioClip());
        }
        
        void play();
        void stop();
        
        //! loads a audio file
        void loadAudio(std::string path);
        
        //! Returns the length of the clip in minutes + seconds
        float getClipLength();
        
        //! returns the length of the clip in seconds
        double getClipLengthInSeconds();
        
        //! returns the current point in the clip in minutes + seconds
        float getCurrentTime();
        
        double getCurrentTimeInSeconds();
    };
}

#endif /* AudioClip_hpp */
