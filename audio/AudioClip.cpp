//
//  AudioClip.cpp
//  MVVJ
//
//  Created by Joseph Chow on 2/27/16.
//
//

#include "AudioClip.hpp"

using namespace ci;
using namespace std;
using namespace ci::app;

namespace moca {
    AudioClip::AudioClip(){}
    
    void AudioClip::loadAudio(std::string path){
        auto ctx = audio::Context::master();
        
        // create a SourceFile and set its output samplerate to match the Context.
        audio::SourceFileRef sourceFile = audio::load( loadAsset(path), ctx->getSampleRate() );
        
        // load the entire sound file into a BufferRef, and construct a BufferPlayerNode with this.
        audio::BufferRef buffer = sourceFile->loadBuffer();
        mBufferPlayerNode = ctx->makeNode( new audio::BufferPlayerNode( buffer ) );
        
        
        // add a Gain to reduce the volume
        mGain = ctx->makeNode( new audio::GainNode( 0.5f ) );
        
        // connect and enable the Context
        mBufferPlayerNode >> mGain >> ctx->getOutput();
        ctx->enable();

    }
    
    void AudioClip::play(){
        mBufferPlayerNode->start();
    }
    
    void AudioClip::stop(){
        if(mBufferPlayerNode->isEnabled()){
            mBufferPlayerNode->stop();
        }
    }
    
    float AudioClip::getClipLength(){
        auto len = mBufferPlayerNode->getNumSeconds();
        
        return len / 60.0;
    }
    
    double AudioClip::getClipLengthInSeconds() {
        return mBufferPlayerNode->getNumSeconds();
    }
    
    float AudioClip::getCurrentTime(){
        auto len = mBufferPlayerNode->getReadPositionTime();
        return len / 60.0;
    }
    
    double AudioClip::getCurrentTimeInSeconds(){
        return mBufferPlayerNode->getReadPositionTime();
    }
    
}