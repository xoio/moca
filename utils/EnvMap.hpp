//
//  EnvMap.hpp
//  StarGazing
//  Port of Yi-Wen's enviroment/cube map technique found here
//  http://blog.bongiovi.tw/simple-environment-map/
//  Created by Joseph Chow on 4/21/16.
//
//

#ifndef EnvMap_hpp
#define EnvMap_hpp

typedef std::shared_ptr<class EnvMap> EnvRef;

class EnvMap {
    ci::gl::VboMeshRef mMesh;
    ci::gl::GlslProgRef mShader;
    ci::gl::BatchRef mBatch;
    
    float numSegments;
    float size;
    
    ci::gl::TextureRef starstexture;
    
    ci::CameraPersp mCamera;
    
    ci::vec3 getPosition(float i,float j, bool isNormal=false);
    
    //! fbo used to give a slight blur
    ci::gl::FboRef blurFbo;
    
public:
    EnvMap(int numSegments,int size);
    
    static EnvRef create(int numSegments=24,int size=1800){
        return EnvRef(new EnvMap(numSegments,size));
    }
    void setup(std::string texturePath);
    void draw();
};

#endif /* EnvMap_hpp */
