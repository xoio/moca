//
//  utils.h
//  MVVJ
//
//  Created by Joseph Chow on 2/27/16.
//
//

#ifndef utils_h
#define utils_h
#include <sstream>
#include <utility>
#include "cinder/Log.h"

namespace moca {
    static std::vector<std::string> explode(std::string const &s,char delim){
        std::vector<std::string> result;
        std::istringstream iss(s);
        for(std::string token;std::getline(iss,token,delim);){
            if(!token.empty()){
                result.push_back(std::move(token));
            }
        }
        
        return result;
    }
}

#endif /* utils_h */
