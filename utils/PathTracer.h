//
//  PathTracer.h
//  Paths
//
//  Created by Joseph Chow on 8/1/15.
//
//

#ifndef __Paths__PathTracer__
#define __Paths__PathTracer__


#include "cinder/BSpline.h"
#include <vector>

typedef std::shared_ptr<class PathTracer> PathTracerRef;

class PathTracer {
    
    //vector to store points where spline needs to draw
    std::vector<ci::vec3> points;
    std::vector<ci::vec2> points2d;
public:
    PathTracer();

    static PathTracerRef create(){
        return PathTracerRef(new PathTracer());
    }
    void setPoints(std::vector<ci::vec3> points);
    void setPoints(std::vector<ci::vec2> points);
    
    
    
};

#endif /* defined(__Paths__PathTracer__) */
