//
//  EnvMap.cpp
//  StarGazing
//
//  Created by Joseph Chow on 4/21/16.
//
//

#include "EnvMap.hpp"
using namespace ci;
using namespace std;
using namespace ci::app;

EnvMap::EnvMap(int numSegments,int size):numSegments(numSegments),size(size){}

vec3 EnvMap::getPosition(float i, float j, bool isNormal){
    float rx = i/numSegments * M_PI - M_PI * 0.5;
    float ry = j/numSegments * M_PI * 2;
    float r = size;
    
    ci::vec3 pos;
    
    pos.y = sin(rx) * r;
    float t = cos(rx) * r;
    pos.x = cos(ry) * t;
    pos.z = sin(ry) * t;
    
    
    return pos;
    
}
void EnvMap::setup(std::string texturepath){
    
    //mCamera.setPerspective( 60.0f, getWindowAspectRatio(), 0.1f, 6000.0f );
    //mCamera.lookAt(vec3(0));
    
    
    gl::GlslProg::Format fmt;
    fmt.vertex(loadAsset("env-vertex.glsl"));
    fmt.fragment(loadAsset("env-fragment.glsl"));
    
    mShader = gl::GlslProg::create(fmt);
    
    vector<vec3> positions;
    vector<vec2> coords;
    std::vector<uint16_t>	indices;
    
    int index = 0;
    float gapUV = 1/numSegments;
    
    for(int i=0; i<numSegments; i++) {
        for(int j=0; j<numSegments; j++) {
            positions.push_back(getPosition(i, j));
            positions.push_back(getPosition(i+1, j));
            positions.push_back(getPosition(i+1, j+1));
            positions.push_back(getPosition(i, j+1));
            
            float u = j / numSegments;
            float v = i / numSegments;
            
            coords.push_back(vec2(1.0 - u, v));
            coords.push_back(vec2(1.0 - u, v+gapUV));
            coords.push_back(vec2(1.0 - u - gapUV, v+gapUV));
            coords.push_back(vec2(1.0 - u - gapUV, v));
            
            indices.push_back(index*4 + 3);
            indices.push_back(index*4 + 2);
            indices.push_back(index*4 + 0);
            indices.push_back(index*4 + 2);
            indices.push_back(index*4 + 1);
            indices.push_back(index*4 + 0);
            
            index++;
        }
    }
    
    gl::VboMesh::Layout layout;
    layout.usage(GL_STATIC_DRAW);
    layout.attrib(geom::POSITION, 3);
    layout.attrib(geom::TEX_COORD_0, 2);
    
    mMesh = gl::VboMesh::create(positions.size(), GL_TRIANGLES, {layout},indices.size(),GL_UNSIGNED_SHORT);
    
    mMesh->bufferAttrib(geom::POSITION, positions);
    mMesh->bufferAttrib(geom::TEX_COORD_0, coords);
    mMesh->bufferIndices( indices.size() * sizeof(uint16_t), indices.data());
    
    mBatch = gl::Batch::create(mMesh, mShader);
    
    starstexture = gl::Texture::create(loadImage(loadAsset("textures/starsmap.jpg")));
    
}

void EnvMap::draw(){
    gl::ScopedGlslProg shd(mShader);
    gl::ScopedTextureBind tex0(starstexture,0);
    mShader->uniform("stars",0);
    mShader->uniform("axis",vec3(0.9170600771903992, 0.39874908328056335, 0));
    
    mBatch->draw();
}
