//
//  PathTracer.cpp
//  Paths
//
//  Created by Joseph Chow on 8/1/15.
//
//

#include "PathTracer.h"
PathTracer::PathTracer(){}


//https://forum.libcinder.org/topic/advices-for-my-3d-project#23286000001019012
void PathTracer::setPoints(std::vector<ci::vec3> points){
 
    //set spline
    ci::BSpline3f path = ci::BSpline3f(points,3,true,false);
    
    //create high resolution list of point sin path
    float resolution = 1000.0f;
    float delta = 1.0f / resolution;
    
    for(float theta = 0.0f; theta < 1.0f; theta += delta){
        this->points.push_back(path.getPosition(theta));
    }
    
}

void PathTracer::setPoints(std::vector<ci::vec2> points){
    
    //set spline
    ci::BSpline2f path = ci::BSpline2f(points,3,true,false);
    
    //create high resolution list of point sin path
    float resolution = 1000.0f;
    float delta = 1.0f / resolution;
    
    for(float theta = 0.0f; theta < 1.0f; theta += delta){
        this->points2d.push_back(path.getPosition(theta));
    }

}