//
//  core.h
//  DynaType
//
//  Created by Joseph Chow on 2/19/16.
//
//

#ifndef core_h
#define core_h
#include "CinderOpenCV.h"
using namespace ci;
using namespace std;
using namespace ci::app;

typedef struct{
    //! list of the raw contours found from OpenCV
    std::vector<std::vector<cv::Point> > contours;
    
    //! a list of points after conversion from the raw set
    std::vector<ci::vec3> points;
    
    void convertToVec(){
        vector<vector<cv::Point> >::iterator it;
        
        for(it = contours.begin();it != contours.end();++it){
            vector<cv::Point> pt = *it;
            vector<cv::Point>::iterator pts;
            for(pts = pt.begin();pts != pt.end();++pts){
                
                points.push_back(vec3(ci::fromOcv(*pts),1.0));
            }
        }
    }
    
}ContourSet;


typedef struct{
    std::vector<cv::Point> cvpoints;
}PointSet;

static cv::Mat toGray(cv::Mat img){
    cv::Mat thresh;
    if(img.channels() == 1) {
        thresh = img.clone();
    } else if(img.channels() == 3) {
        cv::cvtColor(img, thresh, CV_RGB2GRAY);
    } else if(img.channels() == 4) {
        cv::cvtColor(img, thresh, CV_RGBA2GRAY);
    }
    
    return thresh;
}

#endif /* core_h */
