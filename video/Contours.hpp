//
//  Contours.hpp
//  DynaType
//
//  Created by Joseph Chow on 2/18/16.
//
//

#ifndef Contours_hpp
#define Contours_hpp

#include "video/core.h"

using namespace ci;
using namespace std;
using namespace ci::app;


namespace moca {
    namespace ocv {
        
        //! Basic contour finding function. Finds the edges/contours from a ci::Channel
        //! Returns a ContourSet as the result after converting the raw cv::Point values to ci::cec2
        static ContourSet findContours(ci::Channel image,float threshold=128.0,int mode=CV_RETR_LIST,int method=CV_CHAIN_APPROX_NONE){
            ContourSet set;
            
            cv::Mat img(ci::toOcv(image));
            cv::Mat thresh = toGray(img);
            
            //using edge detection at the same time gives us slightly better results.
            cv::Mat canny_output;
            cv::Canny(thresh, canny_output, threshold, threshold * 2,3);
            
            //run contour finding algorithim.
            cv::findContours( canny_output,set.contours, mode,method);
            set.convertToVec();
            return set;
        }
        
    }
}

#endif /* Contours_hpp */
