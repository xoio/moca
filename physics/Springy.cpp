//
//  Springy.cpp
//  Moca
//
//  Created by Joseph Chow on 2/22/16.
//
//

#include "Springy.hpp"
Springy::Springy(){
    mass = 1.0;
    k = 0.2;
    size = 30.0;
    damp = 0.98;
    
}
void Springy::setPosition(ci::vec3 position){
    this->position = position;
    this->tempPos = position;
    this->restPos = position;
}

void Springy::setSize(float size){
    this->size = size;
}

void Springy::setDampening(float damp){
    this->damp = damp;
}

void Springy::update(){
    force = -k * (tempPos.y - restPos.y);
    accel = force / mass;
    velocity.y = damp * (velocity.y + accel);
    tempPos.y += velocity.y;
    
    force = -k * (tempPos.x - restPos.x);
    accel = force / mass;
    velocity.x = damp * (velocity.x + accel);
    tempPos.x += velocity.x;
}