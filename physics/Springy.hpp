//
//  Springy.hpp
//  Moca
//  Simple 2d spring. Extend in your own object class in order to use.
//  Based on Processing example
//  Created by Joseph Chow on 2/22/16.
//
//

#ifndef Springy_hpp
#define Springy_hpp

#include <stdio.h>

class Springy {

protected:
    float rest;
    float size;
    float k;
    float damp;
    float mass;
    float accel;
    float force;
    
    ci::vec3 velocity;
    ci::vec3 restPos;
    ci::vec3 position;
    ci::vec3 tempPos;
    
public:
    Springy();
    void setPosition(ci::vec3 position);
    void setSize(float size);
    void setDampening(float damp);
    void update();
    
};

#endif /* Springy_hpp */
