# m.o.c.a #

aka - modules of computational additives(name change or perhaps just straight up eliminating the acronym 
will happen :poop:)

These are just a series of classes and other various functions for Cinder I've found useful and worth remembering and studying. 

It's arranged as header only, there shouldn't be anything to build. I've categorized things as best as possible. There are some "core" things like default shaders for PBR and noise but for the most part, you should be able to pick and choose things very much like you might a npm module.

Currently arranged by category but will likely switch things up at some point so that related files sit together and re-arrange the file structure to resemble modules instead of categories.

Commented as much as possible for now. 

### Setup ###
Paths are structured in a way that this needs to sit within another folder in the root of your project. ie


```javascript

root
libs
  moca
   <all the files, etc>



```


### Possible gotchas ###

* Relying on boost's filesystem module to resolve paths to core files. Haven't really tested the current implementation yet on a wider variety of situations so it could break.

* this should in no way be considered c++ best practices :poop:
* some old test code might still exist