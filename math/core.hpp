//
//  core.hpp
//  Moca
//
//  Created by Joseph Chow on 2/25/16.
//
//

#ifndef core_h
#define core_h

namespace moca {
    namespace math{
        
        
        
        static float getMax(float x, float y){
            return (((x) > (y)) ? (x) : (y));
        }
        
        static float norm(float value, float min, float max){
            return (value - min) / (max - min);
        }
        
        static float map(float value, float min1, float max1, float min2, float max2){
            return lerp(norm(value,min1,max1),min2,max2);
        }
        
        static float lerp(float val, float min, float max){
            return min + (max - min) * val;
        }
        
        template<class T>
        const T& constrain(const T& x, const T& a, const T& b){
            if(x < a){
                return a;
            }else if(b < x){
                return b;
            }else {
                return x;
            }
            
        }
        
        inline float constrain(float x, float a , float b){
            if(x < a){
                return a;
            }else if(b < x ){
                return b;
            }else {
                return x;
            }
        }
        

    }
    
    namespace vec {
        //! getRotated function taken from openFrameworks
        //! takes
        //! 1. vec3 to rotate
        //! 2. the angle to rotate
        //! 3 and the axis to rotate by
        static ci::vec3 getRotated(ci::vec3 toRotate,float angle,const ci::vec3 axis){
            ci::vec3 ax = glm::normalize(axis);
            
            
            float a = (float)(angle*(M_PI / 180));
            float sina = sin( a );
            float cosa = cos( a );
            float cosb = 1.0f - cosa;
            
            return ci::vec3( toRotate.x * (ax.x*ax.x*cosb + cosa)
                            + toRotate.y *(ax.x*ax.y*cosb - ax.z*sina)
                            + toRotate.z *(ax.x*ax.z*cosb + ax.y*sina),
                            toRotate.x*(ax.y*ax.x*cosb + ax.z*sina)
                            + toRotate.y*(ax.y*ax.y*cosb + cosa)
                            + toRotate.z*(ax.y*ax.z*cosb - ax.x*sina),
                            toRotate.x*(ax.z*ax.x*cosb - ax.y*sina)
                            + toRotate.y*(ax.z*ax.y*cosb + ax.x*sina)
                            + toRotate.z*(ax.z*ax.z*cosb + cosa) );
            
        }

    }
}

#endif /* core_h */
