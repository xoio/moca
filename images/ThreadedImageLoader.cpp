//
//  ThreadedImageLoader.cpp
//  Moca
//
//  Created by Joseph Chow on 2/18/16.
//
//

#include "ThreadedImageLoader.hpp"
using namespace ci;
using namespace std;
using namespace ci::app;
ThreadedImageLoader::ThreadedImageLoader(){
    doneLoading = false;
}

void ThreadedImageLoader::loadImages(){
    gl::ContextRef backgroundCtx = gl::Context::create(gl::context());
    mThread = make_shared<thread>(bind(&ThreadedImageLoader::loadPayload, this, backgroundCtx) );
}

void ThreadedImageLoader::setPayload(std::vector<std::string> urls){
    this->urls = urls;
}

void ThreadedImageLoader::loadPayload(ci::gl::ContextRef context){
    ThreadSetup threadSetup;
    context->makeCurrent();
}