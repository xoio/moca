//
//  ThreadedImageLoader.hpp
//  Moca
//  Basic threaded image loader for loading external images.
//  Follows example in FlickrTestMultithreaded example
//  Created by Joseph Chow on 2/18/16.
//
//

#ifndef ThreadedImageLoader_hpp
#define ThreadedImageLoader_hpp


#include "cinder/ConcurrentCircularBuffer.h"
#include "cinder/Thread.h"

class ThreadedImageLoader {
    
    
    //! Thread object for helping to load all of the images
    std::shared_ptr<std::thread> mThread;
    
    //! Concurrent buffer for queueing and handling loading in a orderly fashion
    ci::ConcurrentCircularBuffer<ci::gl::TextureRef> * mBuffer;
    
    //! loads the payload of images
    void loadPayload(ci::gl::ContextRef context);
    
    //! list of urls to load
    std::vector<std::string> urls;
    
    //! flag to set when we're done loading
    bool doneLoading;
public:
    ThreadedImageLoader();
    void setPayload(std::vector<std::string> urls);
    void loadImages();
};

#endif /* ThreadedImageLoader_hpp */
