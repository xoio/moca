//
//  Text.cpp
//  MVVJ
//
//  Created by Joseph Chow on 2/26/16.
//
//

#include "TextNode.hpp"
using namespace ci;
using namespace std;
using namespace ci::app;

TextNode::TextNode(){
    fontSize = 12.0;
    copy = "";
    textColor = Color::white();
    fontName = "";
}

TextNode::TextNode(std::string copy){
    fontSize = 12.0;
    this->copy = copy;
    textColor = Color::white();
    fontName = "";
}

void TextNode::setFontSize(float fontSize){
    this->fontSize = fontSize;
    
    //if we've already loaded a font before, reload
    if(fontName != ""){
        loadFont(this->fontName);
    }
}

void TextNode::loadFont(std::string fontName){
    auto found = false;
    this->fontName = fontName;
    
    try {
        mFont = Font(loadAsset(fontName),fontSize);
        
        found = true;
    } catch (...) {
        CI_LOG_E("Unable to load Font from assets folder...trying to find file");
    }
    
    
    if(!found){
        try {
            auto font = loadFile(fontName);
            mFont = Font(font,fontSize);
        } catch (...) {
            
            //if the copy was set - mention what the copy was to make it easier to debug
            if(copy != ""){
                throw Exception("Unable to load font for the copy :" + copy + " - after trying from after trying the assets folder and the system");
            }else{
                //otherwise just toss an exception
                throw Exception("Unable to load font from after trying the assets folder and the system");
            }
        }
    }
    
    mTextureFont = gl::TextureFont::create(mFont);
    bounds = Rectf( position.x, mTextureFont->getAscent() + position.y, 400 , 0 );
    
}


void TextNode::setTextBounds(ci::Rectf bounds){
    this->bounds = bounds;
}

void TextNode::draw(){
    gl::pushMatrices();
    
    textColor.a = mAlpha;
    
    gl::color(textColor);
    gl::setMatricesWindow(getWindowSize());
    mTextureFont->drawStringWrapped(copy, bounds);
    gl::popMatrices();
}