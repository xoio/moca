//
//  Text.hpp
//  MVVJ
//
//  Created by Joseph Chow on 2/26/16.
//
//

#ifndef Text_hpp
#define Text_hpp

#include "cinder/Text.h"
#include "cinder/Rand.h"
#include "cinder/gl/TextureFont.h"
#include "cinder/Timeline.h"
#include "moca/core/SceneNode.hpp"

class TextNode : public moca::SceneNode {
    
    ci::Font				mFont;
    ci::gl::TextureFontRef	mTextureFont;

    std::string copy;
    float fontSize;
    
    ci::Rectf bounds;
    
    ci::ColorA textColor;
    std::string fontName;
    
public:
    TextNode();
    TextNode(std::string copy);
    
    //! loads a font file. Assumes fonts are in your assets folder but will also attempt
    //! to load a file directly. 
    void loadFont(std::string fontName);
    
    
    void setFontSize(float fontSize);
    void setTextBounds(ci::Rectf bounds);
    void draw();
};

#endif /* Text_hpp */
