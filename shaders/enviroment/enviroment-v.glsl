#version 150
// vertex shader to use in conjunction with QuickCubemap class
uniform mat4 ciModelViewProjection;
uniform mat4 ciModelView;
uniform mat4 ciProjectionMatrix;

in vec4 ciPosition;
in vec2 ciTexCoord0;

uniform vec3 axis;
uniform float angle;
uniform float near;
uniform float far;

out vec2 vTextureCoord;
out float vDepth;

float getDepth(float z, float n, float f) {
    return (2.0 * n) / (f + n - z*(f-n));
}

vec4 quat_from_axis_angle(vec3 axis, float angle) {
    vec4 qr;
    float half_angle = (angle * 0.5) * 3.14159 / 180.0;
    qr.x = axis.x * sin(half_angle);
    qr.y = axis.y * sin(half_angle);
    qr.z = axis.z * sin(half_angle);
    qr.w = cos(half_angle);
    return qr;
}

vec3 rotate_vertex_position(vec3 position, vec3 axis, float angle) {
    vec4 q = quat_from_axis_angle(axis, angle);
    vec3 v = position.xyz;
    return v + 2.0 * cross(q.xyz, cross(q.xyz, v) + q.w * v);
}

void main(void) {
    vec3 pos = rotate_vertex_position(ciPosition.xyz, axis, angle);
    vec4 V = ciProjectionMatrix * (ciModelView * vec4(pos, 1.0));
    gl_Position = V;
    //gl_Position = ciModelViewProjection * ciPosition;
    
    vTextureCoord = ciTexCoord0;
    
    float d       = getDepth(V.z/V.w, 1.0, 10000.0);
    vDepth        = d;
}