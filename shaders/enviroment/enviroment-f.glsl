#version 150
// fragment shader to use in conjunction with QuickCubemap class
uniform sampler2D stars;
in vec2 vTextureCoord;
in float vDepth;

const vec3 FOG_COLOR = vec3(243.0, 230.0, 214.0)/255.0;

out vec4 glFragColor;

void main(void) {
    vec4 color = texture(stars, vTextureCoord);
    
    glFragColor = color;
    //glFragColor = vec4(1.);
    // float d = pow(vDepth+.15, 4.0);
    // gl_FragColor.rgb = mix(gl_FragColor.rgb, FOG_COLOR, d);
}