uniform mat4 ciModelViewProjection;

in vec4 	ciPosition;
in vec2 	ciTexCoord0;

out vec2 vUv;
out vec4 vPosition;


void main( void )
{
    vUv	= ciTexCoord0;
    vPosition = ciPosition;
    gl_Position = ciModelViewProjection * ciPosition;
}
