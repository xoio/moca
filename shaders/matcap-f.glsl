#version 150
precision highp float;
in vec2	TexCoord;
in vec4	Color;
in vec3 tNormal;
in vec2	Normal;
uniform sampler2D tMatCap;
out vec4 glFragColor;
void main(){
    
    vec3 base = texture(tMatCap,Normal).rgb;
    glFragColor = vec4(base,1.0);
}