#version 150
//using technique described by Jaume Sanchez(@thespite)
//https://www.clicktorelease.com/blog/creating-spherical-environment-mapping-shader
// per-vertex example
uniform mat4	ciModelViewProjection;
uniform mat3	ciNormalMatrix;
uniform mat4    ciModelView;

uniform mat3 normalMatrix;

in vec4		ciPosition;
in vec2		ciTexCoord0;
in vec3		ciNormal;
in vec4		ciColor;

out vec2	TexCoord;
out vec4	Color;
out vec2	Normal;
out vec3 tNormal;

void main( void )
{
    
    
    vec4 p = ciPosition;
    vec3 e = normalize(vec3(ciModelView * p));
    vec3 n = normalize(ciNormalMatrix * ciNormal);
    
    vec3 r = reflect(e,n);
    
    float m = 2. * sqrt(pow(r.x,2.) + pow(r.y,2.) + pow(r.z + 1.,2.));
    
    
    
    gl_Position	= ciModelViewProjection * ciPosition;
    Color 		= ciColor;
    TexCoord	= ciTexCoord0;
    Normal		= r.xy / m + .5;
}
