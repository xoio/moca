#version 150
//using technique described by Jaume Sanchez(@thespite)
//https://www.clicktorelease.com/blog/creating-spherical-environment-mapping-shader
// per-vertex example
uniform mat4	ciModelViewProjection;
uniform mat3	ciNormalMatrix;
uniform mat4    ciModelView;

uniform mat3 normalMatrix;

in vec4		ciPosition;
in vec2		ciTexCoord0;
in vec3		ciNormal;
in vec4		ciColor;

out vec2	TexCoord;
out vec4	Color;
out vec2	Normal;
out vec3    normalInterp;
out vec3    vPosition;

void main( void )
{
     
    
    gl_Position	= ciModelViewProjection * ciPosition;
    vec4 vertPos4 = ciModelView * ciPosition;
    Color 		= ciColor;
    TexCoord	= ciTexCoord0;
    vPosition = vec3(vertPos4) / vertPos4.w;
    normalInterp = ciNormalMatrix * vec3(Normal,1.0);
}
