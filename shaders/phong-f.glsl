#version 150

uniform vec3 ambientLight;
uniform vec3 lightPos;
uniform vec3 diffuseColor;
uniform vec3 specColor;

uniform int mode;

in vec3 normalInterp;
in vec3 vPosition;

out vec4 glFragColor;
void main(){

    vec3 normal = normalize(normalInterp);
    vec3 lightDir = normalize(lightPos - vPosition);
    
    float lambertian = max(dot(lightDir,normal), 0.0);
    float specular = 0.0;
    
    if(lambertian > 0.0) {
        
        vec3 reflectDir = reflect(-lightDir, normal);
        vec3 viewDir = normalize(-vPosition);
        
        float specAngle = max(dot(reflectDir, viewDir), 0.0);
        specular = pow(specAngle, 4.0);
        
        // the exponent controls the shininess (try mode 2)
        if(mode == 2)  specular = pow(specAngle, 16.0);
        
        // according to the rendering equation we would need to multiply
        // with the the "lambertian", but this has little visual effect
        if(mode == 3) specular *= lambertian;
        
        // switch to mode 4 to turn off the specular component
        if(mode == 4) specular *= 0.0;
        
    }
    
    glFragColor = vec4(lambertian*diffuseColor + specular*specColor, 1.0);
}