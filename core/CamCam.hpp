//
//  CamCam.hpp
//  DJVJ
//
//  Created by Joseph Chow on 5/14/16.
//
//

#ifndef CamCam_hpp
#define CamCam_hpp

#include "cinder/Camera.h"
#include "cinder/CameraUi.h"
#include "cinder/Timeline.h"

typedef std::shared_ptr<class CamCam> CamCamRef;

class CamCam {
    ci::CameraPersp mCam;
    ci::CameraUi mCamUi;
    ci::Anim<ci::vec2> mMouse;
    ci::vec3 target;
    
    
public:
    CamCam(float fov, float near, float far, ci::vec3 eye, ci::vec3 target);
    
    static CamCamRef create(float fov=60.0, float near=0.1, float far=1000.0,ci::vec3 eye=ci::vec3(0, 0, 50),ci::vec3 target=ci::vec3(0)){
        return CamCamRef(new CamCam(fov,near,far,eye,target));
    }
    void setFar(float far);
    void setNear(float near);
    void setZoom(float zoom);
    void setAsMatrix();
};

#endif /* CamCam_hpp */
