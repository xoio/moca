//
//  SystemEventManager.hpp
//  NonsenseDrawingMachine
//  Based on the SystemEventManager class by Simon Geilfus for Grove.
//  Includes support for desktop events as well as IOS detection in addition to Android
//  Created by Joseph Chow on 3/27/16.
//
//

#ifndef SystemEventManager_hpp
#define SystemEventManager_hpp
#include <random>

#ifdef CINDER_ANDROID
#define CINDER_MOBILE
#endif

//TODO does this work on all IOS versions? Do we need the above if this is a more general check
#ifdef CINDER_GL_ES 
#define CINDER_MOBILE
#endif

struct DesktopMouseEvent {
    DesktopMouseEvent();
    
    uint32_t    mId;
    double		mTimeOfBirth;
    double		mTimeOfDeath;
    float		mDistanceTraveled;
    ci::vec2	mPos2d, mPrevPos2d;
    ci::vec3	mPos3d;
    bool mIsDoubleClick;
    
    void setDeathTime(double time){
        mTimeOfDeath = time;
    }
};

struct Touch {
    
    Touch();
    
    uint32_t    mId;
    double		mTimeOfBirth;
    double		mTimeOfDeath;
    float		mDistanceTraveled;
    ci::vec2	mPos2d, mPrevPos2d, mInitialPos2d;
    ci::vec3	mPos3d;
    bool		mIsPan, mIsDoubleTap;
};


typedef std::shared_ptr<class SystemEventManager> SystemEventManagerRef;

class SystemEventManager {
public:
    //! create ref to new instance of SystemEventManager
    static SystemEventManagerRef create( const ci::app::WindowRef &window );
    
    //! constructor
    SystemEventManager( const ci::app::WindowRef &window );
    
    void update();
    float generateRandomId();
    
    //--------- TOUCH EVENTS ---------------
#ifdef CINDER_ANDROID
    GestureSignal&	getTapSignal() { return mTapSignal; }
    GestureSignal&	getDoubleTapSignal() { return mDoubleTapSignal; }
    
    void touchesBegan( ci::app::TouchEvent &event );
    void touchesMoved( ci::app::TouchEvent &event );
    void touchesEnded( ci::app::TouchEvent &event );
    std::map<uint32_t, Touch>	mTouches;
    GestureSignal				mTapSignal;
    GestureSignal				mDoubleTapSignal;
#endif
    
    
    
    
    //--------- DESKTOP RELATED -----------
#ifdef CINDER_MAC 
    void mouseClick(cinder::app::MouseEvent event);
    void mouseUp(ci::app::MouseEvent event);
    //! similar to GestureSignal - deals with mouse based events
    typedef ci::signals::Signal<void(const DesktopMouseEvent&)> MouseSignal;
    
    std::vector<DesktopMouseEvent> mouseEvents;
    //std::map<uint32_t,DesktopMouseEvent> mouseEvents;
    MouseSignal singleClick;
    MouseSignal doubleClick;
    
    MouseSignal& getClickSignal(){
        return singleClick;
    }
    
#endif
};


#endif /* SystemEventManager_hpp */
