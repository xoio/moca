//
//  SystemEventManager.cpp
//  NonsenseDrawingMachine
//
//  Created by Joseph Chow on 3/27/16.
//
//

#include "SystemEventManager.hpp"
using namespace std;
using namespace ci;
using namespace ci::app;
DesktopMouseEvent::DesktopMouseEvent():
mTimeOfBirth( getElapsedSeconds() ),
mTimeOfDeath( -1.0f ),
mIsDoubleClick( false )
{}
SystemEventManagerRef SystemEventManager::create(const WindowRef &window){
    return make_shared<SystemEventManager>(window);
}
SystemEventManager::SystemEventManager( const ci::app::WindowRef &window )
{
    App::get()->getSignalUpdate().connect( bind( &SystemEventManager::update, this ) );
#ifdef CINDER_MOBILE
    window->getSignalTouchesBegan().connect( -5, bind( &SystemEventManager::touchesBegan, this, placeholders::_1 ) );
    window->getSignalTouchesMoved().connect( -5, bind( &SystemEventManager::touchesMoved, this, placeholders::_1 ) );
    window->getSignalTouchesEnded().connect( -5, bind( &SystemEventManager::touchesEnded, this, placeholders::_1 ) );
#endif
    
#ifdef CINDER_MAC
    window->getSignalMouseDown().connect(-5, bind( &SystemEventManager::mouseClick, this, placeholders::_1 ));
    //window->getSignalMouseMove().connect(-5, bind( &SystemEventManager::mouseMoved, this, placeholders::_1 ));
    window->getSignalMouseUp().connect(-5, bind( &SystemEventManager::mouseUp, this, placeholders::_1 ));
    
#endif
    
}

//------------ KEY EVENTS --------------
void SystemEventManager::update(){

    
#ifdef CINDER_MAC
    
    for( auto mEvent = mouseEvents.begin(); mEvent != mouseEvents.end(); ){
        bool dead			= (*mEvent).mTimeOfDeath > 0.0f;
        float duration		= getElapsedSeconds() - (*mEvent).mTimeOfBirth;
        
        // we have a completed mouse click
        if( dead && getElapsedSeconds() - (*mEvent).mTimeOfDeath > 0.25 ){
            
            // marked as a double click
            if( (*mEvent).mIsDoubleClick ){
                app::console()<<"double click \n";
            }
            // it's not marked as a double click, so it's a single click
            else if( duration > 0.15f && duration < 0.5f ){
                app::console()<<"single click \n";
            }
            // erase the click
            mEvent = mouseEvents.erase( mEvent );
            continue;
        }
        ++mEvent;
    }
    
#endif
    
}

float SystemEventManager::generateRandomId(){
    float min = 0.0;
    float max = 99999999999999999.0;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(min,max);
    
    return dis(gen);
}

//------------ DESKTOP EVENTS --------------
void SystemEventManager::mouseClick(MouseEvent event){
    if( event.isHandled() ) return;
    
    //prepare new "click"
    DesktopMouseEvent mEvent = DesktopMouseEvent();
    
    // check if we have a existing touch that is close enough
    int closestId = -1;
    float min = 10000000;
    
    //TODO we might not need this
    float newid = generateRandomId();
    auto eventPos = vec2((float)event.getPos().x,(float)event.getPos().y);
    
    //compare with any exsting mouse events to see if this is a single or double click
    for( auto others : mouseEvents ){
        float dist = length( others.mPos2d - toPixels( eventPos ) );
        if( dist < min ){
            closestId	= others.mId;
            min			= dist;
        }
        
        // this is probably the same touch and a double tap
        if( min < 30.0f ){
            float elapsed = getElapsedSeconds() - others.mTimeOfDeath;
            if( elapsed < 0.3f ) {
                mEvent.mIsDoubleClick= true;
            }
        }
    }
    
  
    
    //add most recent click
    mEvent.mId = newid;
    mEvent.mPos2d = toPixels(eventPos);
   // mEvent.mInitialPos2d = toPixels(eventPos);
    
    mouseEvents.push_back(mEvent);
    //mouseEvents.insert(pair<uint32_t, DesktopMouseEvent>(newid,mEvent));
}

void SystemEventManager::mouseUp(ci::app::MouseEvent event){
    for(int i = 0; i < mouseEvents.size();++i){
        mouseEvents.at(i).mTimeOfDeath = getElapsedSeconds();
    }

}

//------------ MOBILE EVENTS ------------------
#ifdef CINDER_MOBILE
void SystemEventManager::touchesBegan( ci::app::TouchEvent &event )
{
    if( event.isHandled() ) return;
    
    for( auto touch : event.getTouches() ){
        
        // check if we have a existing touch that is close enough
        int closestId = -1;
        float min = 10000000;
        for( auto others : mTouches ){
            float dist = length( others.second.mPos2d - toPixels( touch.getPos() ) );
            if( dist < min ){
                closestId	= others.second.mId;
                min			= dist;
            }
        }
        
        // this is probably the same touch and a double tap
        if( min < 30.0f && !mTouches[closestId].mIsPan ){
            float elapsed = getElapsedSeconds() - mTouches[closestId].mTimeOfDeath;
            if( elapsed < 0.3f ) {
                mTouches[closestId].mIsDoubleTap = true;
                continue;
            }
        }
        
        // update the touch
        if( !mTouches.count( touch.getId() ) ) {
            mTouches[touch.getId()]				= Touch();
        }
        mTouches[touch.getId()].mId				= touch.getId();
        mTouches[touch.getId()].mPos2d			= toPixels( touch.getPos() );
        mTouches[touch.getId()].mInitialPos2d	= toPixels( touch.getPos() );
        mTouches[touch.getId()].mPrevPos2d		= touch.getPrevPos();
    }
}
void SystemEventManager::touchesMoved( ci::app::TouchEvent &event )
{
    if( event.isHandled() ) return;
    for( auto touch : event.getTouches() ){
        auto touchIt = mTouches.find( touch.getId() );
        if( touchIt != mTouches.end() ) {
            // update touch
            mTouches[touch.getId()].mPos2d = toPixels( touch.getPos() );
            mTouches[touch.getId()].mPrevPos2d = touch.getPrevPos();
            float dist = length( mTouches[touch.getId()].mPos2d - mTouches[touch.getId()].mPrevPos2d );
            mTouches[touch.getId()].mDistanceTraveled += dist;
        }
    }
}
void SystemEventManager::touchesEnded( ci::app::TouchEvent &event )
{
    if( event.isHandled() ) return;
    // mark touches for deletion
    for( auto touch : event.getTouches() ){
        auto touchIt = mTouches.find( touch.getId() );
        if( touchIt != mTouches.end() ) {
            uint32_t touchId = (*touchIt).second.mId;
            mTouches[touchId].mTimeOfDeath = getElapsedSeconds();
        }
    }
}
#endif