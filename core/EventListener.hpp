//
//  EventListener.hpp
//  MVVJ
//  A very simple event listener system modeled after the openFrameworks event class.
//  Created by Joseph Chow on 2/27/16.
//
//

#ifndef EventListener_hpp
#define EventListener_hpp

#include "cinder/Signals.h"

template <class T>
class EventListener {
    
    //! signals object to hold callbacks that require a value passed back
    ci::signals::Signal<void (T)> signal;
    
    //! signals object to hold callbacks that don't require a value passed back.
    ci::signals::Signal<void (void)> vsignal;
public:
    EventListener(){}
    
    
    //! alias for the connect function
    template <typename A>
    void addEventListener(void (A::* callbackFunction)(T), A * callbackObject){
        connect(callbackFunction, callbackObject);
    }
    
    //! connect function to link a function to the listener when a value needs to be returned.
    template <typename A>
    void connect(void (A::* callbackFunction)(T), A * callbackObject){
        auto b = std::bind(callbackFunction, callbackObject, std::placeholders::_1);
        signal.connect(b);
    }
    
    //! connect function to link a function to the listener without needing a returned value.
    template <typename A>
    void connect(void (A::* callbackFunction)(), A * callbackObject){
        auto b = std::bind(callbackFunction, callbackObject);
        vsignal.connect(b);
    }
    
    //! alias for the connect function
    template <typename A>
    void addEventListener(void (A::* callbackFunction)(), A * callbackObject){
        connect(callbackFunction, callbackObject);
    }
    
    void emit(){
        vsignal.emit();
    }
    
    void emit(T val){
        signal.emit(val);
    }
};

//! typedef for generic events when we don't necessarily need to deal with values
typedef EventListener<float> Listener;

typedef std::shared_ptr<class EventListener<float> > ListenerRef;

namespace moca {
    inline ListenerRef createListener(){
        return ListenerRef(new Listener());
    }
}

#endif /* EventListener_hpp */
