//
//  CamCam.cpp
//  DJVJ
//
//  Created by Joseph Chow on 5/14/16.
//
//

#include "CamCam.hpp"
using namespace ci;
using namespace std;
using namespace ci::app;

CamCam::CamCam(float fov, float near, float far,vec3 eye, vec3 target){

    mCam = CameraPersp( getWindowWidth(), getWindowHeight(),fov,near,far );
    mCam.lookAt(eye,target);
    mCamUi = CameraUi( &mCam );
    
    this->target = target;

    //there are no mouse events on mobile - so only run this if on desktop
#ifdef CINDER_MAC
    getWindow()->getSignalMouseDown().connect([this](MouseEvent event){
        mMouse.stop();
        mMouse = event.getPos();
        mCamUi.mouseDown( mMouse() );
    });
    
    getWindow()->getSignalMouseDrag().connect([this](MouseEvent event){
       
        timeline().apply(&mMouse,vec2(event.getPos()),1.0f,EaseOutQuint()).updateFn([=]
        {
            mCamUi.mouseDrag( mMouse(), event.isLeftDown(), false, event.isRightDown() );
        });
    });
#endif
}

void CamCam::setFar(float far){
    mCam.setFarClip(far);
}

void CamCam::setNear(float near) {
    mCam.setNearClip(near);
}
void CamCam::setZoom(float zoom){
    auto currentEye = mCam.getEyePoint();
    currentEye.z = zoom;
    mCam.lookAt(currentEye,target);
}

void CamCam::setAsMatrix(){
    gl::setMatrices(mCam);
}