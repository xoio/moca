//
//  SceneNode.hpp
//  MVVJ
//
//  Created by Joseph Chow on 2/26/16.
//
//

#ifndef SceneNode_hpp
#define SceneNode_hpp

#include "cinder/Timeline.h"
#include "cinder/Rand.h"
#include "cinder/Log.h"
#include "moca/core/EventListener.hpp"

enum CORE_TRANSITION_PROP {
    ALPHA_TRANSITION,
    POSITION_TRANSITION
};

namespace moca {
    class SceneNode {
        
    protected:
        //! indicates whether or not the node is visible
        bool mVisible;
        
        //! variable alpha prop
        ci::Anim<float> mAlpha;
        
        //! position of the node
        ci::vec3 position;
        
        //! scale of the node
        ci::vec3 scale;
        
        //! orientation of the node
        ci::quat orientation;
        
        //! matrix to track all transformations applied
        ci::mat4 localTransformations;
        
        ci::ColorA nodeColor;
    public:
        SceneNode();
        ~SceneNode();
        void setColor(ci::ColorA color);
        void setAlpha(float mAlpha);
        void setPosition(ci::vec3 position);
        
        //! triggers a transition on a core transitionable prop
        void triggerCoreTransition(CORE_TRANSITION_PROP prop,float time,std::function<void ()> cb,float delay=0.0);
        void triggerCoreTransition(CORE_TRANSITION_PROP prop,float time,float delay=0.0);
        
        //! Performs a transition from one value to another
        template <typename T>
        void fromTo(CORE_TRANSITION_PROP prop,T &from,T &to,float time);
        
        //! draws the item
        virtual void draw(){}
    };
}

#endif /* SceneNode_hpp */
