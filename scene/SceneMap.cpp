//
//  SceneMap.cpp
//  MVVJ
//
//  Created by Joseph Chow on 2/26/16.
//
//

#include "SceneMap.hpp"
using namespace std;
using namespace ci;
using namespace ci::app;
namespace moca {
    SceneMap::SceneMap(){
        
        //insert core functions into the mapping
        mapping.insert(pair<string,std::function<void()> >("enter",[=]()->void{
            enter();
        }));
        
        mapping.insert(pair<string,std::function<void()> >("exit",[=]()->void{
            exit();
        }));
        
        isShowing = false;
        
        gl::Fbo::Format fmt;
        fmt.setSamples(4);
        fbo = gl::Fbo::create(getWindowWidth(),getWindowHeight(),fmt);
    }
    
    SceneMap::~SceneMap(){}
    
    int SceneMap::getId(){
        return sceneId;
    }
    
    void SceneMap::addToScene(moca::SceneNode child){
        children.push_back(child);
    }
    
    void SceneMap::renderScene(){
        gl::ScopedFramebuffer buffer(fbo);
        
        for(int i = 0; i < children.size();++i){
            children.at(i).draw();
        }
    }
    
    void SceneMap::setBackgroundColor(ci::ColorA backgroundColor){
        this->backgroundColor = backgroundColor;
    }
    
    void SceneMap::trigger(std::string scene){
        auto item = mapping.find(scene);
        item->second();
    }
    
    void SceneMap::addTrigger(std::string key,std::function<void ()> func){
        mapping.insert(pair<string,std::function<void()> >(key,func));
    }
    
    void SceneMap::enter(){
        app::console()<<"Enter function triggered";
    }
    void SceneMap::exit(){}
}