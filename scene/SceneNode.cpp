//
//  SceneNode.cpp
//  MVVJ
//
//  Created by Joseph Chow on 2/26/16.
//
//

#include "SceneNode.hpp"
using namespace ci;
using namespace std;
using namespace ci::app;

namespace moca {
    SceneNode::SceneNode(){
        mVisible = true;
        mAlpha = 1.0;
        position = vec3(0);
        nodeColor = Color::white();
    }

    SceneNode::~SceneNode(){
        
    }
    
    void SceneNode::setColor(ci::ColorA color){
        nodeColor = color;
    }
    
    void SceneNode::setPosition(ci::vec3 position){
        this->position = position;
    }
    
    void SceneNode::setAlpha(float mAlpha){
        this->mAlpha = mAlpha;
       
        if(mAlpha <= 0.0){
            mVisible = false;
        }
        
    }
    
    void SceneNode::triggerCoreTransition(CORE_TRANSITION_PROP prop, float time,std::function<void ()> cb,float delay){
        switch (prop) {
            case ALPHA_TRANSITION:
                if(mVisible){
                    app::timeline().apply( &mAlpha, 0.0f,time).easeFn(EaseInQuint()).finishFn(cb).delay(delay);
                    mVisible = false;
                }else{
              
                    app::timeline().apply( &mAlpha, 1.0f,time).easeFn( EaseInQuint()).finishFn(cb).delay(delay);                  mVisible = true;
                    
                }
                break;
                
            default:
                break;
        }
    }
    
    void SceneNode::triggerCoreTransition(CORE_TRANSITION_PROP prop, float time,float delay){
        switch (prop) {
            case ALPHA_TRANSITION:
                if(mVisible){
                    app::timeline().apply( &mAlpha, 0.0f,time).easeFn(EaseInQuint()).delay(delay);
                    mVisible = false;
                }else{
                    app::timeline().apply( &mAlpha, 1.0f,time).easeFn( EaseInQuint()).delay(delay);
                    mVisible = true;
                }
                break;
                
          
            default:
                break;
        }
    }
    
    template <typename T>
    void SceneNode::fromTo(CORE_TRANSITION_PROP prop, T &from, T&to, float time){
        switch (prop) {
            case POSITION_TRANSITION:
                app::timeline().apply( &position, from, to, time).easeFn(EaseInQuint());
                break;
                
            default:
                break;
        }
    }

}