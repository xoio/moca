//
//  SceneMap.hpp
//  ExperimentPictureBook
//
//  Created by Joseph Chow on 5/14/16.
//
//

#ifndef SceneMap_hpp
#define SceneMap_hpp

#include "moca/scene/SceneNode.hpp"
#include "cinder/Json.h"
#include "moca/core/EventListener.hpp"
#include "moca/core/SystemEventManager.hpp"


namespace moca {
    
    typedef std::shared_ptr<class SceneMap> SceneRef;
    
    class SceneMap {
    protected:
        
        //! a map
        std::map<std::string,std::function<void ()> > mapping;
        
        //! An event listener that can be used by the scene to end itself.
        ListenerRef endSignal;
        
        //! an id for the scene
        int sceneId;
        
        //! boolean flag to indicate whether or not the scene is being shown
        bool isShowing;
        
        //! a background color for the scene
        ci::ColorA backgroundColor;
        
        //! an fbo to render the scene onto
        ci::gl::FboRef fbo;
        
        //! items inside of a scene
        std::vector<moca::SceneNode> children;
        
    public:
        SceneMap();
        ~SceneMap();
        
        static SceneRef create(){
            return SceneRef(new SceneMap());
        }
        
        //! adds a child to the scene
        void addToScene(moca::SceneNode child);
        
        //! triggers the end signal for the scene
        virtual void setEndSignal(ListenerRef endSignal){
            this->endSignal = endSignal;
        }
        
        bool areWeOnThisScene(){
            return isShowing;
        }
        
        void setIsShowing(bool isShowing){
            this->isShowing = isShowing;
        }
        
        void setId(int id){
            this->sceneId = id;
        }
        
        //! Bind SceneMap events to pass information from a SystemEventManagerRef instance
        void bindSystemEvents(SystemEventManagerRef systemEvents){
            
#ifdef CINDER_MAC
        systemEvents->getClickSignal().connect(std::bind( &SceneMap::mouseDown, this, std::placeholders::_1));
        systemEvents->getClickSignal().connect(std::bind( &SceneMap::mouseUp, this, std::placeholders::_1));
        systemEvents->getClickSignal().connect(std::bind( &SceneMap::mouseDragged, this, std::placeholders::_1));
#endif
        }
        
        void setBackgroundColor(ci::ColorA backgroundColor);
        
        //! return the scene id
        int getId();
        
        //! adds a triggering function for the scene.
        //! Takes a key and a lambda function to run
        void addTrigger(std::string key,std::function<void ()> func);
        
        //! triggers a section in the scene.
        //! pass in the name of the scene you want to trigger
        void trigger(std::string scene);
        
        //! load settings relating to the scene from
        //! a json file. TODO needs to be implmented
        void loadSettings(std::string path);
        
        //! a core function of the scene.
        //! should be used to describe what happens
        //! when we enter the scene
        virtual void enter();
        
        virtual void renderScene();
        
#ifdef CINDER_MOBILE
        virtual void touchesBegan(Touch t){}
        virtual void touchesMoved(Touch t){}
        virtual void touchesEnded(Touch t){}
#endif
        
#ifdef CINDER_MAC
        virtual void mouseDown(DesktopMouseEvent t){}
        virtual void mouseUp(DesktopMouseEvent t){}
        virtual void mouseDragged(DesktopMouseEvent t){}
#endif
        
        //! a core function of the scene.
        //! should be used to describe what happens
        //! when we exit the scene
        virtual void exit();
        
        virtual void update(){}
        virtual void draw(){}
        
    };
    
}


#endif /* SceneMap_hpp */
