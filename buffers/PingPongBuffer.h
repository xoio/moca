//
//  PingPongBuffer.h
//  WaterColor
//
//  Created by Joseph Chow on 1/3/16.
//
//

#ifndef __WaterColor__PingPongBuffer__
#define __WaterColor__PingPongBuffer__

namespace moca {
    class PingPongBuffer{
        //! flag for the buffers
        int read;
        int write;
        
        //! fbo for use
        ci::gl::FboRef buffer;
    public:
        PingPongBuffer();
        
        //! allocate a buffer. By default we assume it'll be full screen
        void allocate(int width=ci::app::getWindowWidth(),int height=ci::app::getWindowHeight());
        
        //! begin writing to the buffer
        void begin();
        
        //! end writing to the buffer. The color attachment that gets written to next will
        //! also change as well.
        void end();
        
        //! get the current output from the buffer.
        ci::gl::TextureRef getOutput();
    };
}
#endif /* defined(__WaterColor__PingPongBuffer__) */
