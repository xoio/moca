//
//  PingPongBuffer.cpp
//  WaterColor
//
//  Created by Joseph Chow on 1/3/16.
//
//

#include "PingPongBuffer.h"
using namespace ci;
using namespace std;
#define SIZE 512
namespace moca {
    PingPongBuffer::PingPongBuffer(){
        read = 0;
        write = 1;
    }
    
    void PingPongBuffer::allocate(int width,int height){
        gl::Texture::Format texFmt;
        texFmt.setInternalFormat(GL_RGBA);
        
        
        gl::Fbo::Format fmt;
        fmt.colorTexture();
        fmt.disableDepth();
        fmt.setColorTextureFormat(gl::Texture2d::Format().wrap( GL_CLAMP_TO_EDGE ).internalFormat(GL_RGBA32F_ARB));
        fmt.attachment(GL_COLOR_ATTACHMENT0 + 0, gl::Texture2d::create( width,height,texFmt));
        fmt.attachment(GL_COLOR_ATTACHMENT0 + 1, gl::Texture2d::create(width,height,texFmt));
        
        buffer = gl::Fbo::create(width,height,fmt);
        
        
        gl::ScopedFramebuffer fboBind( buffer );{
            gl::clear(ColorA(0,0,0,0));
            gl::setMatricesWindow( buffer->getSize() );
            gl::viewport( buffer->getSize() );
            for(int i = 0; i < 2; ++i){
                gl::drawBuffer(GL_COLOR_ATTACHMENT0 + i);
                
            }
        }
        
        
    }
    
    
    void PingPongBuffer::begin(){
        buffer->bindFramebuffer();
        gl::drawBuffer(GL_COLOR_ATTACHMENT0 +write);
        buffer->getTexture2d(GL_COLOR_ATTACHMENT0 +read)->bind();
        
        
    }
    
    void PingPongBuffer::end(){
        buffer->getTexture2d(GL_COLOR_ATTACHMENT0 + read)->unbind();
        buffer->unbindFramebuffer();
        std::swap(read,write);
        
    }
    
    gl::TextureRef PingPongBuffer::getOutput(){
        return buffer->getTexture2d(GL_COLOR_ATTACHMENT0 + read);
    }
}